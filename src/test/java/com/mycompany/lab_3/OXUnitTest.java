package com.mycompany.lab_3;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class OXUnitTest {
    
    public OXUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWin_O_Vertical1_output_true(){
        String [][] table ={{"O","O","O"},
                            {"4","5","6"},
                            {"1","2","3"}};
        String currentPlayer ="O";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(true, result);
        
        
    }
    @Test
    public void testCheckWin_O_Vertical2_output_true(){
        String [][] table ={{"7","8","9"},
                            {"O","O","O"},
                            {"1","2","3"}};
        String currentPlayer ="O";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(true, result);
        
        
    }
    @Test
    public void testCheckWin_O_Vertical3_output_true(){
        String [][] table ={{"7","8","9"},
                            {"4","5","6"},
                            {"O","O","O"}};
        String currentPlayer ="O";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(true, result);
    }
 

   @Test
    public void testCheckWin_O_Horizontal1_output_true(){
        String [][] table ={{"O","8","9"},
                            {"O","5","6"},
                            {"O","2","3"}};
        String currentPlayer ="O";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(true, result);
    }
 

   @Test
    public void testCheckWin_O_Horizontal2_output_true(){
        String [][] table ={{"7","O","9"},
                            {"4","O","6"},
                            {"1","O","3"}};
        String currentPlayer ="O";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(true, result);
    }
 

   @Test
    public void testCheckWin_O_Horizontal3_output_true(){
        String [][] table ={{"7","8","O"},
                            {"4","5","O"},
                            {"1","2","O"}};
        String currentPlayer ="O";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(true, result);
    }
    
@Test
    public void testCheckWin_O_Diagonal1_output_true(){
        String [][] table ={{"O","8","9"},
                            {"4","O","6"},
                            {"1","2","O"}};
        String currentPlayer ="O";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(true, result);
    }
 
       @Test
    public void testCheckWin_O_Diagonal2_output_true(){
        String [][] table ={{"7","8","O"},
                            {"4","O","6"},
                            {"O","2","3"}};
        String currentPlayer ="O";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(true, result);
    }
    
 
     @Test
    public void testCheckWin_O_Vertical1_output_false(){
        String [][] table ={{"O","8","O"},
                            {"4","5","6"},
                            {"1","2","3"}};
        String currentPlayer ="O";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(false, result);
        
        
    }
    @Test
    public void testCheckWin_O_Vertical2_output_false(){
        String [][] table ={{"7","8","9"},
                            {"4","O","O"},
                            {"1","2","3"}};
        String currentPlayer ="O";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(false, result);
        
        
    }
    @Test
    public void testCheckWin_O_Vertical3_output_false(){
        String [][] table ={{"7","8","9"},
                            {"4","5","6"},
                            {"O","O","3"}};
        String currentPlayer ="O";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(false, result);
    }
    
    
    @Test
    public void testCheckWin_O_Horizontal1_output_false(){
        String [][] table ={{"O","8","9"},
                            {"4","5","6"},
                            {"O","2","3"}};
        String currentPlayer ="O";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(false, result);
    }
 

   @Test
    public void testCheckWin_O_Horizontal2_output_false(){
        String [][] table ={{"7","8","9"},
                            {"4","O","6"},
                            {"1","O","3"}};
        String currentPlayer ="O";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(false, result);
    }
 

   @Test
    public void testCheckWin_O_Horizontal3_output_false(){
        String [][] table ={{"7","8","O"},
                            {"4","5","O"},
                            {"1","2","3"}};
        String currentPlayer ="O";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(false, result);
    }
    
    
     @Test
     public void testCheckWin_Draw_output1_true(){
        String [][] table ={{"X","O","X"},
                            {"O","O","X"},
                            {"O","X","O"}};
        String currentPlayer ="O";
        boolean result =Lab_3. checkDraw(table );
        assertEquals(true, result);
    }
     @Test
     public void testCheckWin_Draw_output2_true(){
        String [][] table ={{"O","O","X"},
                            {"O","O","X"},
                            {"X","X","O"}};
        String currentPlayer ="O";
        boolean result =Lab_3. checkDraw(table );
        assertEquals(true, result);
    }
     @Test
     public void testCheckWin_Draw_output3_true(){
        String [][] table ={{"O","X","X"},
                            {"X","O","O"},
                            {"O","X","O"}};
        String currentPlayer ="O";
        boolean result =Lab_3. checkDraw(table );
        assertEquals(true, result);
    }
     
      @Test
    public void testCheckWin_X_Vertical1_output_true(){
        String [][] table ={{"X","X","X"},
                            {"4","5","6"},
                            {"1","2","3"}};
        String currentPlayer ="X";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(true, result);
        
        
    }
    @Test
    public void testCheckWin_X_Vertical2_output_true(){
        String [][] table ={{"7","8","9"},
                            {"X","X","X"},
                            {"1","2","3"}};
        String currentPlayer ="X";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(true, result);
        
        
    }
    @Test
    public void testCheckWin_X_Vertical3_output_true(){
        String [][] table ={{"7","8","9"},
                            {"4","5","6"},
                            {"X","X","X"}};
        String currentPlayer ="X";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(true, result);
    }
     @Test
    public void testCheckWin_X_Horizontal1_output_true(){
        String [][] table ={{"X","8","9"},
                            {"X","5","6"},
                            {"X","2","3"}};
        String currentPlayer ="X";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(true, result);
    }
 

   @Test
    public void testCheckWin_X_Horizontal2_output_true(){
        String [][] table ={{"7","X","9"},
                            {"4","X","6"},
                            {"1","X","3"}};
        String currentPlayer ="X";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(true, result);
    }
 

   @Test
    public void testCheckWin_X_Horizontal3_output_true(){
        String [][] table ={{"7","8","X"},
                            {"4","5","X"},
                            {"1","2","X"}};
        String currentPlayer ="X";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWin_X_Diagonal1_output_true(){
        String [][] table ={{"X","8","9"},
                            {"4","X","6"},
                            {"1","2","X"}};
        String currentPlayer ="X";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(true, result);
    }
 
       @Test
    public void testCheckWin_X_Diagonal2_output_true(){
        String [][] table ={{"7","8","X"},
                            {"4","X","6"},
                            {"X","2","3"}};
        String currentPlayer ="X";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(true, result);
    }
    
     @Test
    public void testCheckWin_X_Vertical1_output_false(){
        String [][] table ={{"X","8","X"},
                            {"4","5","6"},
                            {"1","2","3"}};
        String currentPlayer ="X";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(false, result);
        
        
    }
    @Test
    public void testCheckWin_X_Vertical2_output_false(){
        String [][] table ={{"7","8","9"},
                            {"4","X","X"},
                            {"1","2","3"}};
        String currentPlayer ="X";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(false, result);
        
        
    }
    @Test
    public void testCheckWin_X_Vertical3_output_false(){
        String [][] table ={{"7","8","9"},
                            {"4","5","6"},
                            {"X","X","3"}};
        String currentPlayer ="X";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(false, result);
    }
    
     @Test
    public void testCheckWin_X_Horizontal1_output_false(){
        String [][] table ={{"X","8","9"},
                            {"4","5","6"},
                            {"X","2","3"}};
        String currentPlayer ="X";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(false, result);
    }
 

   @Test
    public void testCheckWin_X_Horizontal2_output_false(){
        String [][] table ={{"7","8","9"},
                            {"4","X","6"},
                            {"1","X","3"}};
        String currentPlayer ="X";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(false, result);
    }
 

   @Test
    public void testCheckWin_X_Horizontal3_output_false(){
        String [][] table ={{"7","8","X"},
                            {"4","5","X"},
                            {"1","2","3"}};
        String currentPlayer ="X";
        boolean result =Lab_3.checkWin(table ,currentPlayer);
        assertEquals(false, result);
    }
    
    
    
     
     
    
}

