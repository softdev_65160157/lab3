/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab_3;

/**
 *
 * @author informatics
 */
public class Lab_3 {

    public static void main(String[] args) {
        System.out.println("Hello World!");
    }

    static boolean checkRow(String[][] table, String currentPlayer) {   
            for(int i = 0; i < 3;i++){
                if((table[i][0])==currentPlayer &&(table[i][1])==currentPlayer &&(table[i][2])==currentPlayer  ){
                    return true;
                    }     
            } 
            return false;
    }
     static boolean checkCol(String[][] table, String currentPlayer) {   
            for(int i = 0; i < 3;i++){
                if((table[0][i])==currentPlayer &&(table[1][i])==currentPlayer &&(table[2][i])==currentPlayer  ){
                    return true;
                    }     
            } 
            return false;
    }
     
      static boolean checkX1(String[][] table, String currentPlayer) {   
            for(int i = 0; i < 3;i++){
                if((table[i][2-i])!=currentPlayer  ){
                    return false;
                    }     
            } 
            return true;
    }
     
     static boolean checkX2(String[][] table, String currentPlayer) {   
            for(int i = 0; i < 3;i++){
            if((table[i][i])!=currentPlayer ){
                return false;
                }     
            }
            return true;
    }
     static boolean isDraw(String[][] table) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] != "X" && table[i][j] != "O") {
                    return false;
                }
            }
        }
        return true;
     }
     
    static boolean checkWin(String[][] table, String currentPlayer) {
        if (checkRow(table, currentPlayer)){
            return true;
        }
        if (checkCol(table, currentPlayer)){
             return true;
        }
        if (checkX1(table, currentPlayer)){
             return true;
        }
        if (checkX2(table, currentPlayer)){
             return true;
        }
         return false;
    }
    
       static boolean checkDraw(String[][] table) {
        if (isDraw(table)){
            return true;
        }
         return false;
    }
       
        
    }

